const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

function parseInvestments(investmentsResponse) {
    const { ids, ats, amounts, investors, bankAddresses, froms } = investmentsResponse; 
    const investments = [];
    for (let i = 0; i < ids.length; i++) {
        const oneInvestment = {
            id: ids[i].toString(),
            at: ats[i].toString(),
            amount: amounts[i].toString(),
            investor: investors[i].toString(),
            banker: bankAddresses[i].toString(),
            from: froms[i].toString(),
        }
        investments.push(oneInvestment);
    }
    return investments;
}

module.exports = {
    ZERO_ADDRESS,
    parseInvestments
};

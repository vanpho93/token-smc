const Web3 = require('web3');
const HDWalletProvider = require('truffle-hdwallet-provider');
const { INFURA_API, MNEMONIC } = process.env;
const provider = new HDWalletProvider(MNEMONIC, INFURA_API);
const web3 = new Web3(provider);

module.exports = { web3, INFURA_API };

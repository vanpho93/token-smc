const HDWalletProvider = require('truffle-hdwallet-provider');
const MNEMONIC = 'asthma elite library zebra giggle payment exit apart manual three divide absorb';

module.exports = {
  mocha: { timeout: 100000 },
  compilers: {
    solc: {
      version: '0.4.19',    
      docker: false,
      settings: {
        optimizer: {
          enabled: true,
          runs: 200
        },
        evmVersion: 'byzantium'
      }
    }
  }
}

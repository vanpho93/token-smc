const { equal, ok, deepEqual, notEqual } = require('assert');
const Aunic = artifacts.require('./Aunic.sol');
const BankerFactory = artifacts.require('./BankerFactory.sol');
const Banker = artifacts.require('./Banker.sol');

const { ZERO_ADDRESS } = require('../utils');

contract('Deposit via banker', accounts => {
    let aunic, bankerFactory;

    beforeEach('Init contracts', async () => {
        aunic = await Aunic.new();
        bankerFactory = await BankerFactory.new(aunic.address);
    });

    it('Can deposit via banker', async () => {
        await bankerFactory.createBanker(accounts[1]);
        const bankerAddress = await bankerFactory.bankers(accounts[1]);
        const balanceAccount0Before = await aunic.getbalance(accounts[0]);
        await aunic.transfer(accounts[2], '7000000000000000000000000');
        const balanceAccount2Before = await aunic.getbalance(accounts[2]);
        const result = await aunic.transfer(bankerAddress, '7000000000000000000000000', { from: accounts[2] });
        const balanceAccount0 = await aunic.getbalance(accounts[0]);
        const balanceAccount1 = await aunic.getbalance(accounts[1]);
        const balanceAccount2 = await aunic.getbalance(accounts[2]);
        const balanceAccountBanker = await aunic.getbalance(bankerAddress);
        const expectedBalance = {
            balanceAccount0Before: '10000000000000000000000000',
            balanceAccount2Before: '7000000000000000000000000',
            balanceAccount0: '10000000000000000000000000',
            balanceAccount1: '0',
            balanceAccount2: '0',
            balanceAccountBanker: '0'
        };
        deepEqual({
            balanceAccount0Before: balanceAccount0Before.toString(),
            balanceAccount2Before: balanceAccount2Before.toString(),
            balanceAccount0: balanceAccount0.toString(),
            balanceAccount1: balanceAccount1.toString(),
            balanceAccount2: balanceAccount2.toString(),
            balanceAccountBanker: balanceAccountBanker.toString(),
        }, expectedBalance);
    });
});

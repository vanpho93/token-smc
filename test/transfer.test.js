const { equal, ok, deepEqual, notEqual } = require('assert');
const Aunic = artifacts.require('./Aunic.sol');

const { ZERO_ADDRESS } = require('../utils');

contract('Transfer', accounts => {
    let aunic, bankerFactory;

    beforeEach('Init contracts', async () => {
        aunic = await Aunic.new();
    });

    it('Can transfer aunic', async () => {
        const beforeBalanceAccount0 = await aunic.getbalance(accounts[0]);
        const beforeBalanceAccount1 = await aunic.getbalance(accounts[1]);
        equal(beforeBalanceAccount0.toString(), '10000000000000000000000000');
        equal(beforeBalanceAccount1.toString(), '0');
        await aunic.transfer(accounts[1], '7000000000000000000000000');
        const afterBalanceAccount0 = await aunic.getbalance(accounts[0]);
        const afterBalanceAccount1 = await aunic.getbalance(accounts[1]);
        equal(afterBalanceAccount0.toString(), '3000000000000000000000000');
        equal(afterBalanceAccount1.toString(), '7000000000000000000000000');
    });
});

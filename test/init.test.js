const { equal, ok, deepEqual } = require('assert');
const Aunic = artifacts.require('./Aunic.sol');
const BankerFactory = artifacts.require('./BankerFactory.sol');

contract('Init components', accounts => {
    let aunic, bankerFactory;

    beforeEach('Init contracts', async () => {
        aunic = await Aunic.new();
        bankerFactory = await BankerFactory.new(aunic.address);
    });

    it('Can init contacts', async () => {
        const aunicSymbol = await aunic.symbol();
        equal(aunicSymbol, 'AUC');
        const aunicAddressFromBankerFactory = await bankerFactory.TOKEN_ADDRESS();
        equal(aunic.address, aunicAddressFromBankerFactory);
    });
});

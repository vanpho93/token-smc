const { equal, ok, deepEqual, notEqual } = require('assert');
const Aunic = artifacts.require('./Aunic.sol');
const BankerFactory = artifacts.require('./BankerFactory.sol');
const Banker = artifacts.require('./Banker.sol');

const { ZERO_ADDRESS } = require('../utils');

contract('Create banker', accounts => {
    let aunic, bankerFactory;

    beforeEach('Init contracts', async () => {
        aunic = await Aunic.new();
        bankerFactory = await BankerFactory.new(aunic.address);
    });

    it('Can create banker', async () => {
        await bankerFactory.createBanker(accounts[1]);
        const bankerAddress = await bankerFactory.bankers(accounts[1]);
        notEqual(bankerAddress, ZERO_ADDRESS);
        const banker = await Banker.at(bankerAddress);
        const bankerInvestor = await banker.investor();
        const tokenAddress = await banker.TOKEN_ADDRESS();
        const bankerFactoryAddress = await banker.BANKER_FACTORY();
        const rootWallet = await banker.ROOT_WALLET();
        equal(bankerInvestor, accounts[1]);
        equal(tokenAddress, aunic.address);
        equal(bankerFactoryAddress, bankerFactory.address);
        equal(rootWallet, accounts[0]);
    });
});

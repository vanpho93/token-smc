const { equal, ok, deepEqual, notEqual } = require('assert');
const Aunic = artifacts.require('./Aunic.sol');
const BankerFactory = artifacts.require('./BankerFactory.sol');
const Banker = artifacts.require('./Banker.sol');

const { ZERO_ADDRESS, parseInvestments } = require('../utils');

contract('Deposit via banker', accounts => {
    let aunic, bankerFactory;

    beforeEach('Init contracts', async () => {
        aunic = await Aunic.new();
        bankerFactory = await BankerFactory.new(aunic.address);
    });

    it('Can deposit via banker', async () => {
        await bankerFactory.createBanker(accounts[1]);
        await bankerFactory.createBanker(accounts[2]);
        await bankerFactory.createBanker(accounts[3]);
        await bankerFactory.createBanker(accounts[4]);
        const bankerAddress1 = await bankerFactory.bankers(accounts[1]);
        const bankerAddress2 = await bankerFactory.bankers(accounts[2]);
        const bankerAddress3 = await bankerFactory.bankers(accounts[3]);
        const bankerAddress4 = await bankerFactory.bankers(accounts[4]);
        await aunic.transfer(bankerAddress1, '1000000000000000000000000');
        await aunic.transfer(bankerAddress2, '2000000000000000000000000');
        await aunic.transfer(bankerAddress3, '3000000000000000000000000');
        await aunic.transfer(accounts[4], '3000000000000000000000000');
        await aunic.transfer(bankerAddress4, '2500000000000000000000000', { from: accounts[4] });
        const investments = await bankerFactory.getInvestmentsByIndex(0, 6);
        const parsedInvestments = parseInvestments(investments).map(({ amount, investor, from }) => ({ amount, investor, from }));
        const expectedInvestments = [
            {
                amount: '1000000000000000000000000',
                investor: accounts[1],
                from: accounts[0]
            },
            {
                amount: '2000000000000000000000000',
                investor: accounts[2],
                from: accounts[0]
            },
            {
                amount: '3000000000000000000000000',
                investor: accounts[3],
                from: accounts[0]
            },
            {
                amount: '2500000000000000000000000',
                investor: accounts[4],
                from: accounts[4]
            }
        ];
        deepEqual(parsedInvestments, expectedInvestments);
        const balanceAccount0 = await aunic.getbalance(accounts[0]);
        equal(balanceAccount0.toString(), '9500000000000000000000000');
    });
});

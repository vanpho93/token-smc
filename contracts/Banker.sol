pragma solidity 0.4.19;
import "./Aunic.sol";

contract BankerFactoryInterface {
    function saveDeposit(address from, address investor, address banker, uint256 amount) public;
}

contract Banker {
    address public TOKEN_ADDRESS;
    address public ROOT_WALLET;
    address public BANKER_FACTORY;
    address public investor;
    // new chuyen tien vao banker -> new banker thuoc banker, banker nam trong factory, -> nap tien
    function Banker (address _investor, address tokenAddress, address rootWallet, address bankerFactory) public {
        investor = _investor;
        TOKEN_ADDRESS = tokenAddress;
        ROOT_WALLET = rootWallet;
        BANKER_FACTORY = bankerFactory;
    }

    function tokenFallback(address _from, uint _value) public payable {
        require(msg.sender == TOKEN_ADDRESS);
        sendTokenToRootAccount(_value);
        saveDepositDataIntoFactory(_from, this, _value);
    }

    function sendTokenToRootAccount(uint256 amount) internal {
        Aunic token = Aunic(TOKEN_ADDRESS);
        token.transfer(ROOT_WALLET, amount);
    }

    function saveDepositDataIntoFactory(address _from, address banker, uint256 amount) internal {
        BankerFactoryInterface factory = BankerFactoryInterface(BANKER_FACTORY);
        factory.saveDeposit(_from, investor, banker, amount);
    }
}

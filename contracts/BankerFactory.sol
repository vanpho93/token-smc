pragma solidity 0.4.19;
import "./Aunic.sol";
import "./Banker.sol";

contract BankerFactory is Ownable {
    address public TOKEN_ADDRESS = 0x0;
    mapping(address => address) public bankers; // `investor address` to `banker address`
    mapping(address => bool) public isValidBanker;
    address[] public bankerAddresses;
    bytes32[] public investmentIds;
    mapping (bytes32 => Investment) public investments;

    function BankerFactory (address tokenAddress) public onlyOwner {
        TOKEN_ADDRESS = tokenAddress;
    }

    struct Investment {
        bytes32 id;
        uint256 at;
        uint256 amount;
        address investor;
        address banker;
        address from;
    }

    function createBanker(address investor) public {
        require(bankers[investor] == address(0));
        address newBankerAddress = address(new Banker(investor, TOKEN_ADDRESS, owner, this));
        bankers[investor] = newBankerAddress;
        isValidBanker[newBankerAddress] = true;
        bankerAddresses.push(investor);
    }

    function saveDeposit(address from, address investor, address banker, uint256 amount) public {
        require(isValidBanker[msg.sender]); // `must from a banker address that created by this factory`
        bytes32 id = keccak256(block.number, now, banker, investor);
        Investment memory investment = Investment({
            id: id,
            at: now,
            amount: amount,
            from: from,
            investor: investor,
            banker: banker
        });
        investments[id] = investment;
        investmentIds.push(id);
    }
    
    function getInvestmentById(bytes32 investmentId) public view returns (uint256 at, uint256 amount, address investor, address banker, address from) {
        Investment memory investment = investments[investmentId];
        return (investment.at, investment.amount, investment.investor, investment.banker, investment.from);
    }
    
    function getInvestmentsByIndex(uint256 _from, uint256 _to) public view returns(bytes32[] ids, uint256[] ats, uint256[] amounts, address[] investors, address[] bankAddresses, address[] froms) {
        uint256 length = investmentIds.length;
        _from = _from < 0 ? 0 : _from;
        _to = _to > length - 1 ? length - 1 : _to; 
        uint256 arrayLength = _to - _from + 1;
        ids = new bytes32[](arrayLength);
        ats = new uint256[](arrayLength);
        amounts = new uint256[](arrayLength);
        investors = new address[](arrayLength);
        bankAddresses = new address[](arrayLength);
        froms = new address[](arrayLength);
        for (uint256 i = 0; i < arrayLength; i++) {
            bytes32 id = investmentIds[i + _from];
            ids[i] = id;
            (, ats[i], amounts[i], investors[i], bankAddresses[i], froms[i]) = getInvestmentById(id);
        }
        return (ids, ats, amounts, investors, bankAddresses, froms);
    }
}

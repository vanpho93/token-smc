const removeDir = require('rimraf');
const { web3 } = require('./web3.instance');
const aunicCompiled = require('./build/contracts/Aunic.json');

const Aunic = new web3.eth.Contract(aunicCompiled.abi, null, { data: aunicCompiled.bytecode });

async function start() {
    // deploy contracts
    console.log('*** Start ***');
    const accounts = await web3.eth.getAccounts();
    const sendOption = { from: accounts[0], gasPrice: 16e9 };
    const aunic = await Aunic.deploy().send(sendOption)
    .on('transactionHash', hash => console.log('transaction hash:', hash));
    console.log(`Aunic address: ${aunic.options.address}`);
    removeDir.sync('./build');
    process.exit(0);
}

start();
